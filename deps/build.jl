using Compat

function main()
    cd(joinpath(@__DIR__, "..")) do
        run(`git submodule update --init`)
        run(`make clean all cleanobjs`)
    end
end

main()
