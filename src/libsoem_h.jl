# Automatically generated using Clang.jl wrap_c, version 0.0.0

using Compat

mutable struct ec_timet
    sec::Cuint
    usec::Cuint
end

mutable struct osal_timert
    stop_time::ec_timet
end

const EC_ERROR = -3
const EC_NOFRAME = -1
const EC_OTHERFRAME = -2
const EC_MAXECATFRAME = 1518
const EC_MAXLRWDATA = ((((EC_MAXECATFRAME - 14) - 2) - 10) - 2) - 4
const EC_FIRSTDCDATAGRAM = 20
const EC_BUFSIZE = EC_MAXECATFRAME
const EC_ECATTYPE = 0x1000
const EC_MAXBUF = 16
const EC_TIMEOUTRET = (Cint)(2000)
const EC_TIMEOUTRET3 = EC_TIMEOUTRET * 3
const EC_TIMEOUTSAFE = (Cint)(20000)
const EC_TIMEOUTEEP = (Cint)(20000)
const EC_TIMEOUTTXM = (Cint)(20000)
const EC_TIMEOUTRXM = (Cint)(700000)
const EC_TIMEOUTSTATE = (Cint)(2000000)
const EC_MAXEEPBITMAP = 128
const EC_MAXEEPBUF = EC_MAXEEPBITMAP << 5
const EC_DEFAULTRETRIES = 3

# Skipping MacroDefinition: ETH_HEADERSIZE sizeof ( ec_etherheadert )
# Skipping MacroDefinition: EC_HEADERSIZE sizeof ( ec_comt )
# Skipping MacroDefinition: EC_ELENGTHSIZE sizeof ( Cushort )

#const EC_CMDOFFSET = EC_ELENGTHSIZE

# Skipping MacroDefinition: EC_WKCSIZE sizeof ( Cushort )

const EC_DATAGRAMFOLLOWS = 1 << 15
const EC_ESTAT_R64 = 0x0040
const EC_ESTAT_BUSY = 0x8000
const EC_ESTAT_EMASK = 0x7800
const EC_ESTAT_NACK = 0x2000
const ECT_SII_START = 0x0040
const ECT_SDO_SMCOMMTYPE = 0x1c00
const ECT_SDO_PDOASSIGN = 0x1c10
const ECT_SDO_RXPDOASSIGN = 0x1c12
const ECT_SDO_TXPDOASSIGN = 0x1c13
const ETH_P_ECAT = 0x88a4

# Skipping MacroDefinition: MK_WORD ( msb , lsb ) ( ( ( ( Cushort ) ( msb ) ) << 8 ) | ( lsb ) )
# Skipping MacroDefinition: HI_BYTE ( w ) ( ( w ) >> 8 )
# Skipping MacroDefinition: LO_BYTE ( w ) ( ( w ) & 0x00ff )
# Skipping MacroDefinition: SWAP ( w ) ( ( ( ( w ) & 0xff00 ) >> 8 ) | ( ( ( w ) & 0x00ff ) << 8 ) )
# Skipping MacroDefinition: LO_WORD ( l ) ( ( l ) & 0xffff )
# Skipping MacroDefinition: HI_WORD ( l ) ( ( l ) >> 16 )
# Skipping MacroDefinition: get_unaligned ( ptr ) ( { __typeof__ ( * ( ptr ) ) __tmp ; memcpy ( & __tmp , ( ptr ) , sizeof ( * ( ptr ) ) ) ; __tmp ; } )
# Skipping MacroDefinition: put_unaligned32 ( val , ptr ) ( memcpy ( ( ptr ) , & ( val ) , 4 ) )
# Skipping MacroDefinition: put_unaligned64 ( val , ptr ) ( memcpy ( ( ptr ) , & ( val ) , 8 ) )
# Skipping MacroDefinition: htoes ( A ) ( A )
# Skipping MacroDefinition: htoel ( A ) ( A )
# Skipping MacroDefinition: htoell ( A ) ( A )
# Skipping MacroDefinition: etohs ( A ) ( A )
# Skipping MacroDefinition: etohl ( A ) ( A )
# Skipping MacroDefinition: etohll ( A ) ( A )

const EC_MAXELIST = 64
const EC_MAXNAME = 40
const EC_MAXSLAVE = 200
const EC_MAXGROUP = 2
const EC_MAXIOSEGMENTS = 64
const EC_MAXMBX = 1486
const EC_MAXEEPDO = 0x0200
const EC_MAXSM = 8
const EC_MAXFMMU = 4
const EC_MAXLEN_ADAPTERNAME = 128
const EC_MAX_MAPT = 1
const ECT_MBXPROT_AOE = 0x0001
const ECT_MBXPROT_EOE = 0x0002
const ECT_MBXPROT_COE = 0x0004
const ECT_MBXPROT_FOE = 0x0008
const ECT_MBXPROT_SOE = 0x0010
const ECT_MBXPROT_VOE = 0x0020
const ECT_COEDET_SDO = 0x01
const ECT_COEDET_SDOINFO = 0x02
const ECT_COEDET_PDOASSIGN = 0x04
const ECT_COEDET_PDOCONFIG = 0x08
const ECT_COEDET_UPLOAD = 0x10
const ECT_COEDET_SDOCA = 0x20
const EC_SMENABLEMASK = Float32(0x0fffefff)
const EC_MAXODLIST = 1024
const EC_MAXOELIST = 256
const EC_SOE_DATASTATE_B = 0x01
const EC_SOE_NAME_B = 0x02
const EC_SOE_ATTRIBUTE_B = 0x04
const EC_SOE_UNIT_B = 0x08
const EC_SOE_MIN_B = 0x10
const EC_SOE_MAX_B = 0x20
const EC_SOE_VALUE_B = 0x40
const EC_SOE_DEFAULT_B = 0x80
const EC_SOE_MAXNAME = 60
const EC_SOE_MAXMAPPING = 64
const EC_IDN_MDTCONFIG = 24
const EC_IDN_ATCONFIG = 16
const EC_SOE_LENGTH_1 = 0x00
const EC_SOE_LENGTH_2 = 0x01
const EC_SOE_LENGTH_4 = 0x02
const EC_SOE_LENGTH_8 = 0x03
const EC_SOE_TYPE_BINARY = 0x00
const EC_SOE_TYPE_UINT = 0x01
const EC_SOE_TYPE_INT = 0x02
const EC_SOE_TYPE_HEX = 0x03
const EC_SOE_TYPE_STRING = 0x04
const EC_SOE_TYPE_IDN = 0x05
const EC_SOE_TYPE_FLOAT = 0x06
const EC_SOE_TYPE_PARAMETER = 0x07
const EC_NODEOFFSET = 0x1000
const EC_TEMPNODE = Float32(0x0fff)

const ec_bufT = NTuple{1518, Cuchar}
const ec_etherheadert = Void
const ec_comt = Void

const EC_ERR_OK = (UInt32)(0)
const EC_ERR_ALREADY_INITIALIZED = (UInt32)(1)
const EC_ERR_NOT_INITIALIZED = (UInt32)(2)
const EC_ERR_TIMEOUT = (UInt32)(3)
const EC_ERR_NO_SLAVES = (UInt32)(4)
const EC_ERR_NOK = (UInt32)(5)

const ec_err = Void

const EC_STATE_NONE = (Cushort)(0)
const EC_STATE_INIT = (Cushort)(1)
const EC_STATE_PRE_OP = (Cushort)(2)
const EC_STATE_BOOT = (Cushort)(3)
const EC_STATE_SAFE_OP = (Cushort)(4)
const EC_STATE_OPERATIONAL = (Cushort)(8)
const EC_STATE_ACK = (Cushort)(16)
const EC_STATE_ERROR = (Cushort)(16)

const ec_state = Void

const EC_BUF_EMPTY = (UInt32)(0)
const EC_BUF_ALLOC = (UInt32)(1)
const EC_BUF_TX = (UInt32)(2)
const EC_BUF_RCVD = (UInt32)(3)
const EC_BUF_COMPLETE = (UInt32)(4)

const ec_bufstate = Void

const ECT_BOOLEAN = (UInt32)(1)
const ECT_INTEGER8 = (UInt32)(2)
const ECT_INTEGER16 = (UInt32)(3)
const ECT_INTEGER32 = (UInt32)(4)
const ECT_UNSIGNED8 = (UInt32)(5)
const ECT_UNSIGNED16 = (UInt32)(6)
const ECT_UNSIGNED32 = (UInt32)(7)
const ECT_REAL32 = (UInt32)(8)
const ECT_VISIBLE_STRING = (UInt32)(9)
const ECT_OCTET_STRING = (UInt32)(10)
const ECT_UNICODE_STRING = (UInt32)(11)
const ECT_TIME_OF_DAY = (UInt32)(12)
const ECT_TIME_DIFFERENCE = (UInt32)(13)
const ECT_DOMAIN = (UInt32)(15)
const ECT_INTEGER24 = (UInt32)(16)
const ECT_REAL64 = (UInt32)(17)
const ECT_INTEGER64 = (UInt32)(21)
const ECT_UNSIGNED24 = (UInt32)(22)
const ECT_UNSIGNED64 = (UInt32)(27)
const ECT_BIT1 = (UInt32)(48)
const ECT_BIT2 = (UInt32)(49)
const ECT_BIT3 = (UInt32)(50)
const ECT_BIT4 = (UInt32)(51)
const ECT_BIT5 = (UInt32)(52)
const ECT_BIT6 = (UInt32)(53)
const ECT_BIT7 = (UInt32)(54)
const ECT_BIT8 = (UInt32)(55)

const ec_datatype = Void

const EC_CMD_NOP = (UInt32)(0)
const EC_CMD_APRD = (UInt32)(1)
const EC_CMD_APWR = (UInt32)(2)
const EC_CMD_APRW = (UInt32)(3)
const EC_CMD_FPRD = (UInt32)(4)
const EC_CMD_FPWR = (UInt32)(5)
const EC_CMD_FPRW = (UInt32)(6)
const EC_CMD_BRD = (UInt32)(7)
const EC_CMD_BWR = (UInt32)(8)
const EC_CMD_BRW = (UInt32)(9)
const EC_CMD_LRD = (UInt32)(10)
const EC_CMD_LWR = (UInt32)(11)
const EC_CMD_LRW = (UInt32)(12)
const EC_CMD_ARMW = (UInt32)(13)
const EC_CMD_FRMW = (UInt32)(14)

const ec_cmdtype = Void

const EC_ECMD_NOP = (UInt32)(0)
const EC_ECMD_READ = (UInt32)(256)
const EC_ECMD_WRITE = (UInt32)(513)
const EC_ECMD_RELOAD = (UInt32)(768)

const ec_ecmdtype = Void

const ECT_SII_STRING = (Cushort)(10)
const ECT_SII_GENERAL = (Cushort)(30)
const ECT_SII_FMMU = (Cushort)(40)
const ECT_SII_SM = (Cushort)(41)
const ECT_SII_PDO = (Cushort)(50)

const ECT_SII_MANUF = (Cushort)(8)
const ECT_SII_ID = (Cushort)(10)
const ECT_SII_REV = (Cushort)(12)
const ECT_SII_BOOTRXMBX = (Cushort)(20)
const ECT_SII_BOOTTXMBX = (Cushort)(22)
const ECT_SII_MBXSIZE = (Cushort)(25)
const ECT_SII_TXMBXADR = (Cushort)(26)
const ECT_SII_RXMBXADR = (Cushort)(24)
const ECT_SII_MBXPROTO = (Cushort)(28)

const ECT_MBXT_ERR = (UInt32)(0)
const ECT_MBXT_AOE = (UInt32)(1)
const ECT_MBXT_EOE = (UInt32)(2)
const ECT_MBXT_COE = (UInt32)(3)
const ECT_MBXT_FOE = (UInt32)(4)
const ECT_MBXT_SOE = (UInt32)(5)
const ECT_MBXT_VOE = (UInt32)(15)

const ECT_COES_EMERGENCY = (UInt32)(1)
const ECT_COES_SDOREQ = (UInt32)(2)
const ECT_COES_SDORES = (UInt32)(3)
const ECT_COES_TXPDO = (UInt32)(4)
const ECT_COES_RXPDO = (UInt32)(5)
const ECT_COES_TXPDO_RR = (UInt32)(6)
const ECT_COES_RXPDO_RR = (UInt32)(7)
const ECT_COES_SDOINFO = (UInt32)(8)

const ECT_SDO_DOWN_INIT = (UInt32)(33)
const ECT_SDO_DOWN_EXP = (UInt32)(35)
const ECT_SDO_DOWN_INIT_CA = (UInt32)(49)
const ECT_SDO_UP_REQ = (UInt32)(64)
const ECT_SDO_UP_REQ_CA = (UInt32)(80)
const ECT_SDO_SEG_UP_REQ = (UInt32)(96)
const ECT_SDO_ABORT = (UInt32)(128)

const ECT_GET_ODLIST_REQ = (UInt32)(1)
const ECT_GET_ODLIST_RES = (UInt32)(2)
const ECT_GET_OD_REQ = (UInt32)(3)
const ECT_GET_OD_RES = (UInt32)(4)
const ECT_GET_OE_REQ = (UInt32)(5)
const ECT_GET_OE_RES = (UInt32)(6)
const ECT_SDOINFO_ERROR = (UInt32)(7)

const ECT_FOE_READ = (UInt32)(1)
const ECT_FOE_WRITE = (UInt32)(2)
const ECT_FOE_DATA = (UInt32)(3)
const ECT_FOE_ACK = (UInt32)(4)
const ECT_FOE_ERROR = (UInt32)(5)
const ECT_FOE_BUSY = (UInt32)(6)

const ECT_SOE_READREQ = (UInt32)(1)
const ECT_SOE_READRES = (UInt32)(2)
const ECT_SOE_WRITEREQ = (UInt32)(3)
const ECT_SOE_WRITERES = (UInt32)(4)
const ECT_SOE_NOTIFICATION = (UInt32)(5)
const ECT_SOE_EMERGENCY = (UInt32)(6)

const ECT_REG_TYPE = (UInt32)(0)
const ECT_REG_PORTDES = (UInt32)(7)
const ECT_REG_ESCSUP = (UInt32)(8)
const ECT_REG_STADR = (UInt32)(16)
const ECT_REG_ALIAS = (UInt32)(18)
const ECT_REG_DLCTL = (UInt32)(256)
const ECT_REG_DLPORT = (UInt32)(257)
const ECT_REG_DLALIAS = (UInt32)(259)
const ECT_REG_DLSTAT = (UInt32)(272)
const ECT_REG_ALCTL = (UInt32)(288)
const ECT_REG_ALSTAT = (UInt32)(304)
const ECT_REG_ALSTATCODE = (UInt32)(308)
const ECT_REG_PDICTL = (UInt32)(320)
const ECT_REG_IRQMASK = (UInt32)(512)
const ECT_REG_RXERR = (UInt32)(768)
const ECT_REG_FRXERR = (UInt32)(776)
const ECT_REG_EPUECNT = (UInt32)(780)
const ECT_REG_PECNT = (UInt32)(781)
const ECT_REG_PECODE = (UInt32)(782)
const ECT_REG_LLCNT = (UInt32)(784)
const ECT_REG_WDCNT = (UInt32)(1090)
const ECT_REG_EEPCFG = (UInt32)(1280)
const ECT_REG_EEPCTL = (UInt32)(1282)
const ECT_REG_EEPSTAT = (UInt32)(1282)
const ECT_REG_EEPADR = (UInt32)(1284)
const ECT_REG_EEPDAT = (UInt32)(1288)
const ECT_REG_FMMU0 = (UInt32)(1536)
const ECT_REG_FMMU1 = (UInt32)(1552)
const ECT_REG_FMMU2 = (UInt32)(1568)
const ECT_REG_FMMU3 = (UInt32)(1584)
const ECT_REG_SM0 = (UInt32)(2048)
const ECT_REG_SM1 = (UInt32)(2056)
const ECT_REG_SM2 = (UInt32)(2064)
const ECT_REG_SM3 = (UInt32)(2072)
const ECT_REG_SM0STAT = (UInt32)(2053)
const ECT_REG_SM1STAT = (UInt32)(2061)
const ECT_REG_SM1ACT = (UInt32)(2062)
const ECT_REG_SM1CONTR = (UInt32)(2063)
const ECT_REG_DCTIME0 = (UInt32)(2304)
const ECT_REG_DCTIME1 = (UInt32)(2308)
const ECT_REG_DCTIME2 = (UInt32)(2312)
const ECT_REG_DCTIME3 = (UInt32)(2316)
const ECT_REG_DCSYSTIME = (UInt32)(2320)
const ECT_REG_DCSOF = (UInt32)(2328)
const ECT_REG_DCSYSOFFSET = (UInt32)(2336)
const ECT_REG_DCSYSDELAY = (UInt32)(2344)
const ECT_REG_DCSYSDIFF = (UInt32)(2348)
const ECT_REG_DCSPEEDCNT = (UInt32)(2352)
const ECT_REG_DCTIMEFILT = (UInt32)(2356)
const ECT_REG_DCCUC = (UInt32)(2432)
const ECT_REG_DCSYNCACT = (UInt32)(2433)
const ECT_REG_DCSTART0 = (UInt32)(2448)
const ECT_REG_DCCYCLE0 = (UInt32)(2464)
const ECT_REG_DCCYCLE1 = (UInt32)(2468)

const EC_ERR_TYPE_SDO_ERROR = (UInt32)(0)
const EC_ERR_TYPE_EMERGENCY = (UInt32)(1)
const EC_ERR_TYPE_PACKET_ERROR = (UInt32)(3)
const EC_ERR_TYPE_SDOINFO_ERROR = (UInt32)(4)
const EC_ERR_TYPE_FOE_ERROR = (UInt32)(5)
const EC_ERR_TYPE_FOE_BUF2SMALL = (UInt32)(6)
const EC_ERR_TYPE_FOE_PACKETNUMBER = (UInt32)(7)
const EC_ERR_TYPE_SOE_ERROR = (UInt32)(8)
const EC_ERR_TYPE_MBX_ERROR = (UInt32)(9)
const EC_ERR_TYPE_FOE_FILE_NOTFOUND = (UInt32)(10)

const ec_err_type = Void
const ec_errort = Void
const ec_stackT = Void
const ecx_redportt = Void
const ecx_portt = Void
const ec_adaptert = Void

mutable struct ec_adapter
    name::NTuple{128, UInt8}
    desc::NTuple{128, UInt8}
    next::Ptr{ec_adaptert}
end

struct ec_fmmut
    packed::NTuple{16, Cuchar}
end

struct ec_smt
    #StartAddr::Cushort
    #SMlength::Cushort
    #SMflags::Cuint
    packed::NTuple{8, Cuchar}
end
@assert(sizeof(ec_smt) == 8)

mutable struct ec_slavet
    state::Cushort
    ALstatuscode::Cushort
    configadr::Cushort
    aliasadr::Cushort
    eep_man::Cuint
    eep_id::Cuint
    eep_rev::Cuint
    Itype::Cushort
    Dtype::Cushort
    Obits::Cushort
    Obytes::Cuint
    outputs::Ptr{Cuchar}
    Ostartbit::Cuchar
    Ibits::Cushort
    Ibytes::Cuint
    inputs::Ptr{Cuchar}
    Istartbit::Cuchar
    SM::NTuple{8, ec_smt}
    SMtype::NTuple{8, Cuchar}
    FMMU::NTuple{4, ec_fmmut}
    FMMU0func::Cuchar
    FMMU1func::Cuchar
    FMMU2func::Cuchar
    FMMU3func::Cuchar
    mbx_l::Cushort
    mbx_wo::Cushort
    mbx_rl::Cushort
    mbx_ro::Cushort
    mbx_proto::Cushort
    mbx_cnt::Cuchar
    hasdc::Cuchar
    ptype::Cuchar
    topology::Cuchar
    activeports::Cuchar
    consumedports::Cuchar
    parent::Cushort
    parentport::Cuchar
    entryport::Cuchar
    DCrtA::Cint
    DCrtB::Cint
    DCrtC::Cint
    DCrtD::Cint
    pdelay::Cint
    DCnext::Cushort
    DCprevious::Cushort
    DCcycle::Cint
    DCshift::Cint
    DCactive::Cuchar
    configindex::Cushort
    SIIindex::Cushort
    eep_8byte::Cuchar
    eep_pdi::Cuchar
    CoEdetails::Cuchar
    FoEdetails::Cuchar
    EoEdetails::Cuchar
    SoEdetails::Cuchar
    Ebuscurrent::Cshort
    blockLRW::Cuchar
    group::Cuchar
    FMMUunused::Cuchar
    islost::Cuchar
    PO2SOconfig::Ptr{Void}
    name::NTuple{41, Cchar}
end
@assert(sizeof(ec_slavet) == 328)

mutable struct ec_groupt
    logstartaddr::Cuint
    Obytes::Cuint
    outputs::Ptr{Cuchar}
    Ibytes::Cuint
    inputs::Ptr{Cuchar}
    hasdc::Cuchar
    DCnext::Cushort
    Ebuscurrent::Cshort
    blockLRW::Cuchar
    nsegments::Cushort
    Isegment::Cushort
    Ioffset::Cushort
    outputsWKC::Cushort
    inputsWKC::Cushort
    docheckstate::Cuchar
    IOsegment::NTuple{64, Cuint}
end

mutable struct ec_eepromFMMUt
    Startpos::Cushort
    nFMMU::Cuchar
    FMMU0::Cuchar
    FMMU1::Cuchar
    FMMU2::Cuchar
    FMMU3::Cuchar
end

mutable struct ec_eepromSMt
    Startpos::Cushort
    nSM::Cuchar
    PhStart::Cushort
    Plength::Cushort
    Creg::Cuchar
    Sreg::Cuchar
    Activate::Cuchar
    PDIctrl::Cuchar
end

mutable struct ec_eepromPDOt
    Startpos::Cushort
    Length::Cushort
    nPDO::Cushort
    Index::NTuple{512, Cushort}
    SyncM::NTuple{512, Cushort}
    BitSize::NTuple{512, Cushort}
    SMbitsize::NTuple{8, Cushort}
end

const ec_mbxbuft = NTuple{1487, Cuchar}
const ec_mbxheadert = Void
const ec_alstatust = Void

mutable struct ec_idxstack
    pushed::Cuchar
    pulled::Cuchar
    idx::NTuple{16, Cuchar}
    data::NTuple{16, Ptr{Void}}
    length::NTuple{16, Cushort}
end

const ec_idxstackT = Void

mutable struct ec_ering
    head::Cshort
    tail::Cshort
    Error::NTuple{65, ec_errort}
end

const ec_eringt = Void
const ec_SMcommtypet = Void
const ec_PDOassignt = Void
const ec_PDOdesct = Void

mutable struct ecx_contextt
    port::Ptr{ecx_portt}
    slavelist::Ptr{ec_slavet}
    slavecount::Ptr{Cint}
    maxslave::Cint
    grouplist::Ptr{ec_groupt}
    maxgroup::Cint
    esibuf::Ptr{Cuchar}
    esimap::Ptr{Cuint}
    esislave::Cushort
    elist::Ptr{ec_eringt}
    idxstack::Ptr{ec_idxstackT}
    ecaterror::Ptr{Cuchar}
    DCtO::Cushort
    DCl::Cushort
    DCtime::Ptr{Clonglong}
    SMcommtype::Ptr{ec_SMcommtypet}
    PDOassign::Ptr{ec_PDOassignt}
    PDOdesc::Ptr{ec_PDOdesct}
    eepSM::Ptr{ec_eepromSMt}
    eepFMMU::Ptr{ec_eepromFMMUt}
    FOEhook::Ptr{Void}
end

const ec_ODlistt = Void
const ec_OElistt = Void
const ec_SoEnamet = Void
const ec_SoElistt = Void
const ec_SoEmappingt = Void
const ec_SoEattributet = Void
