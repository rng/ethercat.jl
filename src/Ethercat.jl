module Ethercat

include("libsoem.jl")

# State management

function state_change(slave, state, timeout)
    s = ec_slave(slave)
    s.state = state
    ec_slave!(slave, s)
    ec_writestate(Cushort(slave))
    nstate = ec_statecheck(Cushort(slave), state, timeout)
    if nstate != state
        error(@sprintf("state changed failed req=%d actual=%d", state, nstate))
    end
end

end
