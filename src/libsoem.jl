# Julia wrapper for header: ../SOEM/soem/ethercat.h
# Automatically generated using Clang.jl wrap_c, version 0.0.0

include("libsoem_h.jl")

const libsoem = "./libsoem.so"

# ----

_EcatError = cglobal((:EcatError, libsoem), Cuchar)
_ec_slavecount = cglobal((:ec_slavecount, libsoem), Cint)
_ec_slave = cglobal((:ec_slave, libsoem), ec_slavet)

function EcatError()
    Bool(unsafe_load(_EcatError))
end

function ec_slavecount()
    unsafe_load(_ec_slavecount)
end

function ec_slave(idx)
    unsafe_load(_ec_slave, idx+1)
end

function ec_slave!(idx, slave)
    unsafe_store!(_ec_slave, slave, idx+1)
end

# ----

function osal_timer_start(self, timeout_us::Cuint)
    ccall((:osal_timer_start, libsoem), Void, (Ptr{osal_timert}, Cuint), self, timeout_us)
end

function osal_timer_is_expired(self)
    ccall((:osal_timer_is_expired, libsoem), Cuchar, (Ptr{osal_timert},), self)
end

function osal_usleep(usec::Cuint)
    ccall((:osal_usleep, libsoem), Cint, (Cuint,), usec)
end

function osal_current_time()
    ccall((:osal_current_time, libsoem), ec_timet, ())
end

function osal_time_diff(start, _end, diff)
    ccall((:osal_time_diff, libsoem), Void, (Ptr{ec_timet}, Ptr{ec_timet}, Ptr{ec_timet}), start, _end, diff)
end

function osal_thread_create(thandle, stacksize::Cint, func, param)
    ccall((:osal_thread_create, libsoem), Cint, (Ptr{Void}, Cint, Ptr{Void}, Ptr{Void}), thandle, stacksize, func, param)
end

function osal_thread_create_rt(thandle, stacksize::Cint, func, param)
    ccall((:osal_thread_create_rt, libsoem), Cint, (Ptr{Void}, Cint, Ptr{Void}, Ptr{Void}), thandle, stacksize, func, param)
end

function ec_setupnic(ifname, secondary::Cint)
    ccall((:ec_setupnic, libsoem), Cint, (Cstring, Cint), ifname, secondary)
end

function ec_closenic()
    ccall((:ec_closenic, libsoem), Cint, ())
end

function ec_setbufstat(idx::Cint, bufstat::Cint)
    ccall((:ec_setbufstat, libsoem), Void, (Cint, Cint), idx, bufstat)
end

function ec_getindex()
    ccall((:ec_getindex, libsoem), Cint, ())
end

function ec_outframe(idx::Cint, sock::Cint)
    ccall((:ec_outframe, libsoem), Cint, (Cint, Cint), idx, sock)
end

function ec_outframe_red(idx::Cint)
    ccall((:ec_outframe_red, libsoem), Cint, (Cint,), idx)
end

function ec_waitinframe(idx::Cint, timeout::Cint)
    ccall((:ec_waitinframe, libsoem), Cint, (Cint, Cint), idx, timeout)
end

function ec_srconfirm(idx::Cint, timeout::Cint)
    ccall((:ec_srconfirm, libsoem), Cint, (Cint, Cint), idx, timeout)
end

function ec_setupheader(p)
    ccall((:ec_setupheader, libsoem), Void, (Ptr{Void},), p)
end

function ecx_setupnic(port, ifname, secondary::Cint)
    ccall((:ecx_setupnic, libsoem), Cint, (Ptr{ecx_portt}, Cstring, Cint), port, ifname, secondary)
end

function ecx_closenic(port)
    ccall((:ecx_closenic, libsoem), Cint, (Ptr{ecx_portt},), port)
end

function ecx_setbufstat(port, idx::Cint, bufstat::Cint)
    ccall((:ecx_setbufstat, libsoem), Void, (Ptr{ecx_portt}, Cint, Cint), port, idx, bufstat)
end

function ecx_getindex(port)
    ccall((:ecx_getindex, libsoem), Cint, (Ptr{ecx_portt},), port)
end

function ecx_outframe(port, idx::Cint, sock::Cint)
    ccall((:ecx_outframe, libsoem), Cint, (Ptr{ecx_portt}, Cint, Cint), port, idx, sock)
end

function ecx_outframe_red(port, idx::Cint)
    ccall((:ecx_outframe_red, libsoem), Cint, (Ptr{ecx_portt}, Cint), port, idx)
end

function ecx_waitinframe(port, idx::Cint, timeout::Cint)
    ccall((:ecx_waitinframe, libsoem), Cint, (Ptr{ecx_portt}, Cint, Cint), port, idx, timeout)
end

function ecx_srconfirm(port, idx::Cint, timeout::Cint)
    ccall((:ecx_srconfirm, libsoem), Cint, (Ptr{ecx_portt}, Cint, Cint), port, idx, timeout)
end

function ecx_setupdatagram(port, frame, com::Cuchar, idx::Cuchar, ADP::Cushort, ADO::Cushort, length::Cushort, data)
    ccall((:ecx_setupdatagram, libsoem), Cint, (Ptr{ecx_portt}, Ptr{Void}, Cuchar, Cuchar, Cushort, Cushort, Cushort, Ptr{Void}), port, frame, com, idx, ADP, ADO, length, data)
end

function ecx_adddatagram(port, frame, com::Cuchar, idx::Cuchar, more::Cuchar, ADP::Cushort, ADO::Cushort, length::Cushort, data)
    ccall((:ecx_adddatagram, libsoem), Cint, (Ptr{ecx_portt}, Ptr{Void}, Cuchar, Cuchar, Cuchar, Cushort, Cushort, Cushort, Ptr{Void}), port, frame, com, idx, more, ADP, ADO, length, data)
end

function ecx_BWR(port, ADP::Cushort, ADO::Cushort, length::Cushort, data, timeout::Cint)
    ccall((:ecx_BWR, libsoem), Cint, (Ptr{ecx_portt}, Cushort, Cushort, Cushort, Ptr{Void}, Cint), port, ADP, ADO, length, data, timeout)
end

function ecx_BRD(port, ADP::Cushort, ADO::Cushort, length::Cushort, data, timeout::Cint)
    ccall((:ecx_BRD, libsoem), Cint, (Ptr{ecx_portt}, Cushort, Cushort, Cushort, Ptr{Void}, Cint), port, ADP, ADO, length, data, timeout)
end

function ecx_APRD(port, ADP::Cushort, ADO::Cushort, length::Cushort, data, timeout::Cint)
    ccall((:ecx_APRD, libsoem), Cint, (Ptr{ecx_portt}, Cushort, Cushort, Cushort, Ptr{Void}, Cint), port, ADP, ADO, length, data, timeout)
end

function ecx_ARMW(port, ADP::Cushort, ADO::Cushort, length::Cushort, data, timeout::Cint)
    ccall((:ecx_ARMW, libsoem), Cint, (Ptr{ecx_portt}, Cushort, Cushort, Cushort, Ptr{Void}, Cint), port, ADP, ADO, length, data, timeout)
end

function ecx_FRMW(port, ADP::Cushort, ADO::Cushort, length::Cushort, data, timeout::Cint)
    ccall((:ecx_FRMW, libsoem), Cint, (Ptr{ecx_portt}, Cushort, Cushort, Cushort, Ptr{Void}, Cint), port, ADP, ADO, length, data, timeout)
end

function ecx_APRDw(port, ADP::Cushort, ADO::Cushort, timeout::Cint)
    ccall((:ecx_APRDw, libsoem), Cushort, (Ptr{ecx_portt}, Cushort, Cushort, Cint), port, ADP, ADO, timeout)
end

function ecx_FPRD(port, ADP::Cushort, ADO::Cushort, length::Cushort, data, timeout::Cint)
    ccall((:ecx_FPRD, libsoem), Cint, (Ptr{ecx_portt}, Cushort, Cushort, Cushort, Ptr{Void}, Cint), port, ADP, ADO, length, data, timeout)
end

function ecx_FPRDw(port, ADP::Cushort, ADO::Cushort, timeout::Cint)
    ccall((:ecx_FPRDw, libsoem), Cushort, (Ptr{ecx_portt}, Cushort, Cushort, Cint), port, ADP, ADO, timeout)
end

function ecx_APWRw(port, ADP::Cushort, ADO::Cushort, data::Cushort, timeout::Cint)
    ccall((:ecx_APWRw, libsoem), Cint, (Ptr{ecx_portt}, Cushort, Cushort, Cushort, Cint), port, ADP, ADO, data, timeout)
end

function ecx_APWR(port, ADP::Cushort, ADO::Cushort, length::Cushort, data, timeout::Cint)
    ccall((:ecx_APWR, libsoem), Cint, (Ptr{ecx_portt}, Cushort, Cushort, Cushort, Ptr{Void}, Cint), port, ADP, ADO, length, data, timeout)
end

function ecx_FPWRw(port, ADP::Cushort, ADO::Cushort, data::Cushort, timeout::Cint)
    ccall((:ecx_FPWRw, libsoem), Cint, (Ptr{ecx_portt}, Cushort, Cushort, Cushort, Cint), port, ADP, ADO, data, timeout)
end

function ecx_FPWR(port, ADP::Cushort, ADO::Cushort, length::Cushort, data, timeout::Cint)
    ccall((:ecx_FPWR, libsoem), Cint, (Ptr{ecx_portt}, Cushort, Cushort, Cushort, Ptr{Void}, Cint), port, ADP, ADO, length, data, timeout)
end

function ecx_LRW(port, LogAdr::Cuint, length::Cushort, data, timeout::Cint)
    ccall((:ecx_LRW, libsoem), Cint, (Ptr{ecx_portt}, Cuint, Cushort, Ptr{Void}, Cint), port, LogAdr, length, data, timeout)
end

function ecx_LRD(port, LogAdr::Cuint, length::Cushort, data, timeout::Cint)
    ccall((:ecx_LRD, libsoem), Cint, (Ptr{ecx_portt}, Cuint, Cushort, Ptr{Void}, Cint), port, LogAdr, length, data, timeout)
end

function ecx_LWR(port, LogAdr::Cuint, length::Cushort, data, timeout::Cint)
    ccall((:ecx_LWR, libsoem), Cint, (Ptr{ecx_portt}, Cuint, Cushort, Ptr{Void}, Cint), port, LogAdr, length, data, timeout)
end

function ecx_LRWDC(port, LogAdr::Cuint, length::Cushort, data, DCrs::Cushort, DCtime, timeout::Cint)
    ccall((:ecx_LRWDC, libsoem), Cint, (Ptr{ecx_portt}, Cuint, Cushort, Ptr{Void}, Cushort, Ptr{Clonglong}, Cint), port, LogAdr, length, data, DCrs, DCtime, timeout)
end

function ec_setupdatagram(frame, com::Cuchar, idx::Cuchar, ADP::Cushort, ADO::Cushort, length::Cushort, data)
    ccall((:ec_setupdatagram, libsoem), Cint, (Ptr{Void}, Cuchar, Cuchar, Cushort, Cushort, Cushort, Ptr{Void}), frame, com, idx, ADP, ADO, length, data)
end

function ec_adddatagram(frame, com::Cuchar, idx::Cuchar, more::Cuchar, ADP::Cushort, ADO::Cushort, length::Cushort, data)
    ccall((:ec_adddatagram, libsoem), Cint, (Ptr{Void}, Cuchar, Cuchar, Cuchar, Cushort, Cushort, Cushort, Ptr{Void}), frame, com, idx, more, ADP, ADO, length, data)
end

function ec_BWR(ADP::Cushort, ADO::Cushort, length::Cushort, data, timeout::Cint)
    ccall((:ec_BWR, libsoem), Cint, (Cushort, Cushort, Cushort, Ptr{Void}, Cint), ADP, ADO, length, data, timeout)
end

function ec_BRD(ADP::Cushort, ADO::Cushort, length::Cushort, data, timeout::Cint)
    ccall((:ec_BRD, libsoem), Cint, (Cushort, Cushort, Cushort, Ptr{Void}, Cint), ADP, ADO, length, data, timeout)
end

function ec_APRD(ADP::Cushort, ADO::Cushort, length::Cushort, data, timeout::Cint)
    ccall((:ec_APRD, libsoem), Cint, (Cushort, Cushort, Cushort, Ptr{Void}, Cint), ADP, ADO, length, data, timeout)
end

function ec_ARMW(ADP::Cushort, ADO::Cushort, length::Cushort, data, timeout::Cint)
    ccall((:ec_ARMW, libsoem), Cint, (Cushort, Cushort, Cushort, Ptr{Void}, Cint), ADP, ADO, length, data, timeout)
end

function ec_FRMW(ADP::Cushort, ADO::Cushort, length::Cushort, data, timeout::Cint)
    ccall((:ec_FRMW, libsoem), Cint, (Cushort, Cushort, Cushort, Ptr{Void}, Cint), ADP, ADO, length, data, timeout)
end

function ec_APRDw(ADP::Cushort, ADO::Cushort, timeout::Cint)
    ccall((:ec_APRDw, libsoem), Cushort, (Cushort, Cushort, Cint), ADP, ADO, timeout)
end

function ec_FPRD(ADP::Cushort, ADO::Cushort, length::Cushort, data, timeout::Cint)
    ccall((:ec_FPRD, libsoem), Cint, (Cushort, Cushort, Cushort, Ptr{Void}, Cint), ADP, ADO, length, data, timeout)
end

function ec_FPRDw(ADP::Cushort, ADO::Cushort, timeout::Cint)
    ccall((:ec_FPRDw, libsoem), Cushort, (Cushort, Cushort, Cint), ADP, ADO, timeout)
end

function ec_APWRw(ADP::Cushort, ADO::Cushort, data::Cushort, timeout::Cint)
    ccall((:ec_APWRw, libsoem), Cint, (Cushort, Cushort, Cushort, Cint), ADP, ADO, data, timeout)
end

function ec_APWR(ADP::Cushort, ADO::Cushort, length::Cushort, data, timeout::Cint)
    ccall((:ec_APWR, libsoem), Cint, (Cushort, Cushort, Cushort, Ptr{Void}, Cint), ADP, ADO, length, data, timeout)
end

function ec_FPWRw(ADP::Cushort, ADO::Cushort, data::Cushort, timeout::Cint)
    ccall((:ec_FPWRw, libsoem), Cint, (Cushort, Cushort, Cushort, Cint), ADP, ADO, data, timeout)
end

function ec_FPWR(ADP::Cushort, ADO::Cushort, length::Cushort, data, timeout::Cint)
    ccall((:ec_FPWR, libsoem), Cint, (Cushort, Cushort, Cushort, Ptr{Void}, Cint), ADP, ADO, length, data, timeout)
end

function ec_LRW(LogAdr::Cuint, length::Cushort, data, timeout::Cint)
    ccall((:ec_LRW, libsoem), Cint, (Cuint, Cushort, Ptr{Void}, Cint), LogAdr, length, data, timeout)
end

function ec_LRD(LogAdr::Cuint, length::Cushort, data, timeout::Cint)
    ccall((:ec_LRD, libsoem), Cint, (Cuint, Cushort, Ptr{Void}, Cint), LogAdr, length, data, timeout)
end

function ec_LWR(LogAdr::Cuint, length::Cushort, data, timeout::Cint)
    ccall((:ec_LWR, libsoem), Cint, (Cuint, Cushort, Ptr{Void}, Cint), LogAdr, length, data, timeout)
end

function ec_LRWDC(LogAdr::Cuint, length::Cushort, data, DCrs::Cushort, DCtime, timeout::Cint)
    ccall((:ec_LRWDC, libsoem), Cint, (Cuint, Cushort, Ptr{Void}, Cushort, Ptr{Clonglong}, Cint), LogAdr, length, data, DCrs, DCtime, timeout)
end

function ec_pusherror(Ec)
    ccall((:ec_pusherror, libsoem), Void, (Ptr{ec_errort},), Ec)
end

function ec_poperror(Ec)
    ccall((:ec_poperror, libsoem), Cuchar, (Ptr{ec_errort},), Ec)
end

function ec_iserror()
    ccall((:ec_iserror, libsoem), Cuchar, ())
end

function ec_packeterror(Slave::Cushort, Index::Cushort, SubIdx::Cuchar, ErrorCode::Cushort)
    ccall((:ec_packeterror, libsoem), Void, (Cushort, Cushort, Cuchar, Cushort), Slave, Index, SubIdx, ErrorCode)
end

function ec_init(ifname)
    ccall((:ec_init, libsoem), Cint, (Cstring,), ifname)
end

function ec_init_redundant(ifname, if2name)
    ccall((:ec_init_redundant, libsoem), Cint, (Cstring, Cstring), ifname, if2name)
end

function ec_close()
    ccall((:ec_close, libsoem), Void, ())
end

function ec_siigetbyte(slave::Cushort, address::Cushort)
    ccall((:ec_siigetbyte, libsoem), Cuchar, (Cushort, Cushort), slave, address)
end

function ec_siifind(slave::Cushort, cat::Cushort)
    ccall((:ec_siifind, libsoem), Cshort, (Cushort, Cushort), slave, cat)
end

function ec_siistring(str, slave::Cushort, Sn::Cushort)
    ccall((:ec_siistring, libsoem), Void, (Cstring, Cushort, Cushort), str, slave, Sn)
end

function ec_siiFMMU(slave::Cushort, FMMU)
    ccall((:ec_siiFMMU, libsoem), Cushort, (Cushort, Ptr{ec_eepromFMMUt}), slave, FMMU)
end

function ec_siiSM(slave::Cushort, SM)
    ccall((:ec_siiSM, libsoem), Cushort, (Cushort, Ptr{ec_eepromSMt}), slave, SM)
end

function ec_siiSMnext(slave::Cushort, SM, n::Cushort)
    ccall((:ec_siiSMnext, libsoem), Cushort, (Cushort, Ptr{ec_eepromSMt}, Cushort), slave, SM, n)
end

function ec_siiPDO(slave::Cushort, PDO, t::Cuchar)
    ccall((:ec_siiPDO, libsoem), Cint, (Cushort, Ptr{ec_eepromPDOt}, Cuchar), slave, PDO, t)
end

function ec_readstate()
    ccall((:ec_readstate, libsoem), Cint, ())
end

function ec_writestate(slave::Cushort)
    ccall((:ec_writestate, libsoem), Cint, (Cushort,), slave)
end

function ec_statecheck(slave::Cushort, reqstate::Cushort, timeout::Cint)
    ccall((:ec_statecheck, libsoem), Cushort, (Cushort, Cushort, Cint), slave, reqstate, timeout)
end

function ec_mbxempty(slave::Cushort, timeout::Cint)
    ccall((:ec_mbxempty, libsoem), Cint, (Cushort, Cint), slave, timeout)
end

function ec_mbxsend(slave::Cushort, mbx, timeout::Cint)
    ccall((:ec_mbxsend, libsoem), Cint, (Cushort, Ptr{ec_mbxbuft}, Cint), slave, mbx, timeout)
end

function ec_mbxreceive(slave::Cushort, mbx, timeout::Cint)
    ccall((:ec_mbxreceive, libsoem), Cint, (Cushort, Ptr{ec_mbxbuft}, Cint), slave, mbx, timeout)
end

function ec_esidump(slave::Cushort, esibuf)
    ccall((:ec_esidump, libsoem), Void, (Cushort, Ptr{Cuchar}), slave, esibuf)
end

function ec_readeeprom(slave::Cushort, eeproma::Cushort, timeout::Cint)
    ccall((:ec_readeeprom, libsoem), Cuint, (Cushort, Cushort, Cint), slave, eeproma, timeout)
end

function ec_writeeeprom(slave::Cushort, eeproma::Cushort, data::Cushort, timeout::Cint)
    ccall((:ec_writeeeprom, libsoem), Cint, (Cushort, Cushort, Cushort, Cint), slave, eeproma, data, timeout)
end

function ec_eeprom2master(slave::Cushort)
    ccall((:ec_eeprom2master, libsoem), Cint, (Cushort,), slave)
end

function ec_eeprom2pdi(slave::Cushort)
    ccall((:ec_eeprom2pdi, libsoem), Cint, (Cushort,), slave)
end

function ec_readeepromAP(aiadr::Cushort, eeproma::Cushort, timeout::Cint)
    ccall((:ec_readeepromAP, libsoem), Culonglong, (Cushort, Cushort, Cint), aiadr, eeproma, timeout)
end

function ec_writeeepromAP(aiadr::Cushort, eeproma::Cushort, data::Cushort, timeout::Cint)
    ccall((:ec_writeeepromAP, libsoem), Cint, (Cushort, Cushort, Cushort, Cint), aiadr, eeproma, data, timeout)
end

function ec_readeepromFP(configadr::Cushort, eeproma::Cushort, timeout::Cint)
    ccall((:ec_readeepromFP, libsoem), Culonglong, (Cushort, Cushort, Cint), configadr, eeproma, timeout)
end

function ec_writeeepromFP(configadr::Cushort, eeproma::Cushort, data::Cushort, timeout::Cint)
    ccall((:ec_writeeepromFP, libsoem), Cint, (Cushort, Cushort, Cushort, Cint), configadr, eeproma, data, timeout)
end

function ec_readeeprom1(slave::Cushort, eeproma::Cushort)
    ccall((:ec_readeeprom1, libsoem), Void, (Cushort, Cushort), slave, eeproma)
end

function ec_readeeprom2(slave::Cushort, timeout::Cint)
    ccall((:ec_readeeprom2, libsoem), Cuint, (Cushort, Cint), slave, timeout)
end

function ec_send_processdata_group(group::Cuchar)
    ccall((:ec_send_processdata_group, libsoem), Cint, (Cuchar,), group)
end

function ec_send_overlap_processdata_group(group::Cuchar)
    ccall((:ec_send_overlap_processdata_group, libsoem), Cint, (Cuchar,), group)
end

function ec_receive_processdata_group(group::Cuchar, timeout::Cint)
    ccall((:ec_receive_processdata_group, libsoem), Cint, (Cuchar, Cint), group, timeout)
end

function ec_send_processdata()
    ccall((:ec_send_processdata, libsoem), Cint, ())
end

function ec_send_overlap_processdata()
    ccall((:ec_send_overlap_processdata, libsoem), Cint, ())
end

function ec_receive_processdata(timeout::Cint)
    ccall((:ec_receive_processdata, libsoem), Cint, (Cint,), timeout)
end

function ec_find_adapters()
    ccall((:ec_find_adapters, libsoem), Ptr{ec_adaptert}, ())
end

function ec_free_adapters(adapter)
    ccall((:ec_free_adapters, libsoem), Void, (Ptr{ec_adaptert},), adapter)
end

function ec_nextmbxcnt(cnt::Cuchar)
    ccall((:ec_nextmbxcnt, libsoem), Cuchar, (Cuchar,), cnt)
end

function ec_clearmbx(Mbx)
    ccall((:ec_clearmbx, libsoem), Void, (Ptr{ec_mbxbuft},), Mbx)
end

function ecx_pusherror(context, Ec)
    ccall((:ecx_pusherror, libsoem), Void, (Ptr{ecx_contextt}, Ptr{ec_errort}), context, Ec)
end

function ecx_poperror(context, Ec)
    ccall((:ecx_poperror, libsoem), Cuchar, (Ptr{ecx_contextt}, Ptr{ec_errort}), context, Ec)
end

function ecx_iserror(context)
    ccall((:ecx_iserror, libsoem), Cuchar, (Ptr{ecx_contextt},), context)
end

function ecx_packeterror(context, Slave::Cushort, Index::Cushort, SubIdx::Cuchar, ErrorCode::Cushort)
    ccall((:ecx_packeterror, libsoem), Void, (Ptr{ecx_contextt}, Cushort, Cushort, Cuchar, Cushort), context, Slave, Index, SubIdx, ErrorCode)
end

function ecx_init(context, ifname)
    ccall((:ecx_init, libsoem), Cint, (Ptr{ecx_contextt}, Cstring), context, ifname)
end

function ecx_init_redundant(context, redport, ifname, if2name)
    ccall((:ecx_init_redundant, libsoem), Cint, (Ptr{ecx_contextt}, Ptr{ecx_redportt}, Cstring, Cstring), context, redport, ifname, if2name)
end

function ecx_close(context)
    ccall((:ecx_close, libsoem), Void, (Ptr{ecx_contextt},), context)
end

function ecx_siigetbyte(context, slave::Cushort, address::Cushort)
    ccall((:ecx_siigetbyte, libsoem), Cuchar, (Ptr{ecx_contextt}, Cushort, Cushort), context, slave, address)
end

function ecx_siifind(context, slave::Cushort, cat::Cushort)
    ccall((:ecx_siifind, libsoem), Cshort, (Ptr{ecx_contextt}, Cushort, Cushort), context, slave, cat)
end

function ecx_siistring(context, str, slave::Cushort, Sn::Cushort)
    ccall((:ecx_siistring, libsoem), Void, (Ptr{ecx_contextt}, Cstring, Cushort, Cushort), context, str, slave, Sn)
end

function ecx_siiFMMU(context, slave::Cushort, FMMU)
    ccall((:ecx_siiFMMU, libsoem), Cushort, (Ptr{ecx_contextt}, Cushort, Ptr{ec_eepromFMMUt}), context, slave, FMMU)
end

function ecx_siiSM(context, slave::Cushort, SM)
    ccall((:ecx_siiSM, libsoem), Cushort, (Ptr{ecx_contextt}, Cushort, Ptr{ec_eepromSMt}), context, slave, SM)
end

function ecx_siiSMnext(context, slave::Cushort, SM, n::Cushort)
    ccall((:ecx_siiSMnext, libsoem), Cushort, (Ptr{ecx_contextt}, Cushort, Ptr{ec_eepromSMt}, Cushort), context, slave, SM, n)
end

function ecx_siiPDO(context, slave::Cushort, PDO, t::Cuchar)
    ccall((:ecx_siiPDO, libsoem), Cint, (Ptr{ecx_contextt}, Cushort, Ptr{ec_eepromPDOt}, Cuchar), context, slave, PDO, t)
end

function ecx_readstate(context)
    ccall((:ecx_readstate, libsoem), Cint, (Ptr{ecx_contextt},), context)
end

function ecx_writestate(context, slave::Cushort)
    ccall((:ecx_writestate, libsoem), Cint, (Ptr{ecx_contextt}, Cushort), context, slave)
end

function ecx_statecheck(context, slave::Cushort, reqstate::Cushort, timeout::Cint)
    ccall((:ecx_statecheck, libsoem), Cushort, (Ptr{ecx_contextt}, Cushort, Cushort, Cint), context, slave, reqstate, timeout)
end

function ecx_mbxempty(context, slave::Cushort, timeout::Cint)
    ccall((:ecx_mbxempty, libsoem), Cint, (Ptr{ecx_contextt}, Cushort, Cint), context, slave, timeout)
end

function ecx_mbxsend(context, slave::Cushort, mbx, timeout::Cint)
    ccall((:ecx_mbxsend, libsoem), Cint, (Ptr{ecx_contextt}, Cushort, Ptr{ec_mbxbuft}, Cint), context, slave, mbx, timeout)
end

function ecx_mbxreceive(context, slave::Cushort, mbx, timeout::Cint)
    ccall((:ecx_mbxreceive, libsoem), Cint, (Ptr{ecx_contextt}, Cushort, Ptr{ec_mbxbuft}, Cint), context, slave, mbx, timeout)
end

function ecx_esidump(context, slave::Cushort, esibuf)
    ccall((:ecx_esidump, libsoem), Void, (Ptr{ecx_contextt}, Cushort, Ptr{Cuchar}), context, slave, esibuf)
end

function ecx_readeeprom(context, slave::Cushort, eeproma::Cushort, timeout::Cint)
    ccall((:ecx_readeeprom, libsoem), Cuint, (Ptr{ecx_contextt}, Cushort, Cushort, Cint), context, slave, eeproma, timeout)
end

function ecx_writeeeprom(context, slave::Cushort, eeproma::Cushort, data::Cushort, timeout::Cint)
    ccall((:ecx_writeeeprom, libsoem), Cint, (Ptr{ecx_contextt}, Cushort, Cushort, Cushort, Cint), context, slave, eeproma, data, timeout)
end

function ecx_eeprom2master(context, slave::Cushort)
    ccall((:ecx_eeprom2master, libsoem), Cint, (Ptr{ecx_contextt}, Cushort), context, slave)
end

function ecx_eeprom2pdi(context, slave::Cushort)
    ccall((:ecx_eeprom2pdi, libsoem), Cint, (Ptr{ecx_contextt}, Cushort), context, slave)
end

function ecx_readeepromAP(context, aiadr::Cushort, eeproma::Cushort, timeout::Cint)
    ccall((:ecx_readeepromAP, libsoem), Culonglong, (Ptr{ecx_contextt}, Cushort, Cushort, Cint), context, aiadr, eeproma, timeout)
end

function ecx_writeeepromAP(context, aiadr::Cushort, eeproma::Cushort, data::Cushort, timeout::Cint)
    ccall((:ecx_writeeepromAP, libsoem), Cint, (Ptr{ecx_contextt}, Cushort, Cushort, Cushort, Cint), context, aiadr, eeproma, data, timeout)
end

function ecx_readeepromFP(context, configadr::Cushort, eeproma::Cushort, timeout::Cint)
    ccall((:ecx_readeepromFP, libsoem), Culonglong, (Ptr{ecx_contextt}, Cushort, Cushort, Cint), context, configadr, eeproma, timeout)
end

function ecx_writeeepromFP(context, configadr::Cushort, eeproma::Cushort, data::Cushort, timeout::Cint)
    ccall((:ecx_writeeepromFP, libsoem), Cint, (Ptr{ecx_contextt}, Cushort, Cushort, Cushort, Cint), context, configadr, eeproma, data, timeout)
end

function ecx_readeeprom1(context, slave::Cushort, eeproma::Cushort)
    ccall((:ecx_readeeprom1, libsoem), Void, (Ptr{ecx_contextt}, Cushort, Cushort), context, slave, eeproma)
end

function ecx_readeeprom2(context, slave::Cushort, timeout::Cint)
    ccall((:ecx_readeeprom2, libsoem), Cuint, (Ptr{ecx_contextt}, Cushort, Cint), context, slave, timeout)
end

function ecx_send_overlap_processdata_group(context, group::Cuchar)
    ccall((:ecx_send_overlap_processdata_group, libsoem), Cint, (Ptr{ecx_contextt}, Cuchar), context, group)
end

function ecx_receive_processdata_group(context, group::Cuchar, timeout::Cint)
    ccall((:ecx_receive_processdata_group, libsoem), Cint, (Ptr{ecx_contextt}, Cuchar, Cint), context, group, timeout)
end

function ecx_send_processdata(context)
    ccall((:ecx_send_processdata, libsoem), Cint, (Ptr{ecx_contextt},), context)
end

function ecx_send_overlap_processdata(context)
    ccall((:ecx_send_overlap_processdata, libsoem), Cint, (Ptr{ecx_contextt},), context)
end

function ecx_receive_processdata(context, timeout::Cint)
    ccall((:ecx_receive_processdata, libsoem), Cint, (Ptr{ecx_contextt}, Cint), context, timeout)
end

function ec_configdc()
    ccall((:ec_configdc, libsoem), Cuchar, ())
end

function ec_dcsync0(slave::Cushort, act::Cuchar, CyclTime::Cuint, CyclShift::Cint)
    ccall((:ec_dcsync0, libsoem), Void, (Cushort, Cuchar, Cuint, Cint), slave, act, CyclTime, CyclShift)
end

function ec_dcsync01(slave::Cushort, act::Cuchar, CyclTime0::Cuint, CyclTime1::Cuint, CyclShift::Cint)
    ccall((:ec_dcsync01, libsoem), Void, (Cushort, Cuchar, Cuint, Cuint, Cint), slave, act, CyclTime0, CyclTime1, CyclShift)
end

function ecx_configdc(context)
    ccall((:ecx_configdc, libsoem), Cuchar, (Ptr{ecx_contextt},), context)
end

function ecx_dcsync0(context, slave::Cushort, act::Cuchar, CyclTime::Cuint, CyclShift::Cint)
    ccall((:ecx_dcsync0, libsoem), Void, (Ptr{ecx_contextt}, Cushort, Cuchar, Cuint, Cint), context, slave, act, CyclTime, CyclShift)
end

function ecx_dcsync01(context, slave::Cushort, act::Cuchar, CyclTime0::Cuint, CyclTime1::Cuint, CyclShift::Cint)
    ccall((:ecx_dcsync01, libsoem), Void, (Ptr{ecx_contextt}, Cushort, Cuchar, Cuint, Cuint, Cint), context, slave, act, CyclTime0, CyclTime1, CyclShift)
end

function ec_SDOerror(Slave::Cushort, Index::Cushort, SubIdx::Cuchar, AbortCode::Cint)
    ccall((:ec_SDOerror, libsoem), Void, (Cushort, Cushort, Cuchar, Cint), Slave, Index, SubIdx, AbortCode)
end

function ec_SDOread(slave::Cushort, index::Cushort, subindex::Cuchar, CA::Cuchar, psize, p, timeout::Cint)
    ccall((:ec_SDOread, libsoem), Cint, (Cushort, Cushort, Cuchar, Cuchar, Ptr{Cint}, Ptr{Void}, Cint), slave, index, subindex, CA, psize, p, timeout)
end

function ec_SDOwrite(Slave::Cushort, Index::Cushort, SubIndex::Cuchar, CA::Cuchar, psize::Cint, p, Timeout::Cint)
    ccall((:ec_SDOwrite, libsoem), Cint, (Cushort, Cushort, Cuchar, Cuchar, Cint, Ptr{Void}, Cint), Slave, Index, SubIndex, CA, psize, p, Timeout)
end

function ec_RxPDO(Slave::Cushort, RxPDOnumber::Cushort, psize::Cint, p)
    ccall((:ec_RxPDO, libsoem), Cint, (Cushort, Cushort, Cint, Ptr{Void}), Slave, RxPDOnumber, psize, p)
end

function ec_TxPDO(slave::Cushort, TxPDOnumber::Cushort, psize, p, timeout::Cint)
    ccall((:ec_TxPDO, libsoem), Cint, (Cushort, Cushort, Ptr{Cint}, Ptr{Void}, Cint), slave, TxPDOnumber, psize, p, timeout)
end

function ec_readPDOmap(Slave::Cushort, Osize, Isize)
    ccall((:ec_readPDOmap, libsoem), Cint, (Cushort, Ptr{Cint}, Ptr{Cint}), Slave, Osize, Isize)
end

function ec_readPDOmapCA(Slave::Cushort, Thread_n::Cint, Osize, Isize)
    ccall((:ec_readPDOmapCA, libsoem), Cint, (Cushort, Cint, Ptr{Cint}, Ptr{Cint}), Slave, Thread_n, Osize, Isize)
end

function ec_readODlist(Slave::Cushort, pODlist)
    ccall((:ec_readODlist, libsoem), Cint, (Cushort, Ptr{ec_ODlistt}), Slave, pODlist)
end

function ec_readODdescription(Item::Cushort, pODlist)
    ccall((:ec_readODdescription, libsoem), Cint, (Cushort, Ptr{ec_ODlistt}), Item, pODlist)
end

function ec_readOEsingle(Item::Cushort, SubI::Cuchar, pODlist, pOElist)
    ccall((:ec_readOEsingle, libsoem), Cint, (Cushort, Cuchar, Ptr{ec_ODlistt}, Ptr{ec_OElistt}), Item, SubI, pODlist, pOElist)
end

function ec_readOE(Item::Cushort, pODlist, pOElist)
    ccall((:ec_readOE, libsoem), Cint, (Cushort, Ptr{ec_ODlistt}, Ptr{ec_OElistt}), Item, pODlist, pOElist)
end

function ecx_SDOerror(context, Slave::Cushort, Index::Cushort, SubIdx::Cuchar, AbortCode::Cint)
    ccall((:ecx_SDOerror, libsoem), Void, (Ptr{ecx_contextt}, Cushort, Cushort, Cuchar, Cint), context, Slave, Index, SubIdx, AbortCode)
end

function ecx_SDOread(context, slave::Cushort, index::Cushort, subindex::Cuchar, CA::Cuchar, psize, p, timeout::Cint)
    ccall((:ecx_SDOread, libsoem), Cint, (Ptr{ecx_contextt}, Cushort, Cushort, Cuchar, Cuchar, Ptr{Cint}, Ptr{Void}, Cint), context, slave, index, subindex, CA, psize, p, timeout)
end

function ecx_SDOwrite(context, Slave::Cushort, Index::Cushort, SubIndex::Cuchar, CA::Cuchar, psize::Cint, p, Timeout::Cint)
    ccall((:ecx_SDOwrite, libsoem), Cint, (Ptr{ecx_contextt}, Cushort, Cushort, Cuchar, Cuchar, Cint, Ptr{Void}, Cint), context, Slave, Index, SubIndex, CA, psize, p, Timeout)
end

function ecx_RxPDO(context, Slave::Cushort, RxPDOnumber::Cushort, psize::Cint, p)
    ccall((:ecx_RxPDO, libsoem), Cint, (Ptr{ecx_contextt}, Cushort, Cushort, Cint, Ptr{Void}), context, Slave, RxPDOnumber, psize, p)
end

function ecx_TxPDO(context, slave::Cushort, TxPDOnumber::Cushort, psize, p, timeout::Cint)
    ccall((:ecx_TxPDO, libsoem), Cint, (Ptr{ecx_contextt}, Cushort, Cushort, Ptr{Cint}, Ptr{Void}, Cint), context, slave, TxPDOnumber, psize, p, timeout)
end

function ecx_readPDOmap(context, Slave::Cushort, Osize, Isize)
    ccall((:ecx_readPDOmap, libsoem), Cint, (Ptr{ecx_contextt}, Cushort, Ptr{Cint}, Ptr{Cint}), context, Slave, Osize, Isize)
end

function ecx_readPDOmapCA(context, Slave::Cushort, Thread_n::Cint, Osize, Isize)
    ccall((:ecx_readPDOmapCA, libsoem), Cint, (Ptr{ecx_contextt}, Cushort, Cint, Ptr{Cint}, Ptr{Cint}), context, Slave, Thread_n, Osize, Isize)
end

function ecx_readODlist(context, Slave::Cushort, pODlist)
    ccall((:ecx_readODlist, libsoem), Cint, (Ptr{ecx_contextt}, Cushort, Ptr{ec_ODlistt}), context, Slave, pODlist)
end

function ecx_readODdescription(context, Item::Cushort, pODlist)
    ccall((:ecx_readODdescription, libsoem), Cint, (Ptr{ecx_contextt}, Cushort, Ptr{ec_ODlistt}), context, Item, pODlist)
end

function ecx_readOEsingle(context, Item::Cushort, SubI::Cuchar, pODlist, pOElist)
    ccall((:ecx_readOEsingle, libsoem), Cint, (Ptr{ecx_contextt}, Cushort, Cuchar, Ptr{ec_ODlistt}, Ptr{ec_OElistt}), context, Item, SubI, pODlist, pOElist)
end

function ecx_readOE(context, Item::Cushort, pODlist, pOElist)
    ccall((:ecx_readOE, libsoem), Cint, (Ptr{ecx_contextt}, Cushort, Ptr{ec_ODlistt}, Ptr{ec_OElistt}), context, Item, pODlist, pOElist)
end

function ec_FOEdefinehook(hook)
    ccall((:ec_FOEdefinehook, libsoem), Cint, (Ptr{Void},), hook)
end

function ec_FOEread(slave::Cushort, filename, password::Cuint, psize, p, timeout::Cint)
    ccall((:ec_FOEread, libsoem), Cint, (Cushort, Cstring, Cuint, Ptr{Cint}, Ptr{Void}, Cint), slave, filename, password, psize, p, timeout)
end

function ec_FOEwrite(slave::Cushort, filename, password::Cuint, psize::Cint, p, timeout::Cint)
    ccall((:ec_FOEwrite, libsoem), Cint, (Cushort, Cstring, Cuint, Cint, Ptr{UInt8}, Cint), slave, filename, password, psize, p, timeout)
end

function ecx_FOEdefinehook(context, hook)
    ccall((:ecx_FOEdefinehook, libsoem), Cint, (Ptr{ecx_contextt}, Ptr{Void}), context, hook)
end

function ecx_FOEread(context, slave::Cushort, filename, password::Cuint, psize, p, timeout::Cint)
    ccall((:ecx_FOEread, libsoem), Cint, (Ptr{ecx_contextt}, Cushort, Cstring, Cuint, Ptr{Cint}, Ptr{Void}, Cint), context, slave, filename, password, psize, p, timeout)
end

function ecx_FOEwrite(context, slave::Cushort, filename, password::Cuint, psize::Cint, p, timeout::Cint)
    ccall((:ecx_FOEwrite, libsoem), Cint, (Ptr{ecx_contextt}, Cushort, Cstring, Cuint, Cint, Ptr{Void}, Cint), context, slave, filename, password, psize, p, timeout)
end

function ec_SoEread(slave::Cushort, driveNo::Cuchar, elementflags::Cuchar, idn::Cushort, psize, p, timeout::Cint)
    ccall((:ec_SoEread, libsoem), Cint, (Cushort, Cuchar, Cuchar, Cushort, Ptr{Cint}, Ptr{Void}, Cint), slave, driveNo, elementflags, idn, psize, p, timeout)
end

function ec_SoEwrite(slave::Cushort, driveNo::Cuchar, elementflags::Cuchar, idn::Cushort, psize::Cint, p, timeout::Cint)
    ccall((:ec_SoEwrite, libsoem), Cint, (Cushort, Cuchar, Cuchar, Cushort, Cint, Ptr{Void}, Cint), slave, driveNo, elementflags, idn, psize, p, timeout)
end

function ec_readIDNmap(slave::Cushort, Osize, Isize)
    ccall((:ec_readIDNmap, libsoem), Cint, (Cushort, Ptr{Cint}, Ptr{Cint}), slave, Osize, Isize)
end

function ecx_SoEread(context, slave::Cushort, driveNo::Cuchar, elementflags::Cuchar, idn::Cushort, psize, p, timeout::Cint)
    ccall((:ecx_SoEread, libsoem), Cint, (Ptr{ecx_contextt}, Cushort, Cuchar, Cuchar, Cushort, Ptr{Cint}, Ptr{Void}, Cint), context, slave, driveNo, elementflags, idn, psize, p, timeout)
end

function ecx_SoEwrite(context, slave::Cushort, driveNo::Cuchar, elementflags::Cuchar, idn::Cushort, psize::Cint, p, timeout::Cint)
    ccall((:ecx_SoEwrite, libsoem), Cint, (Ptr{ecx_contextt}, Cushort, Cuchar, Cuchar, Cushort, Cint, Ptr{Void}, Cint), context, slave, driveNo, elementflags, idn, psize, p, timeout)
end

function ecx_readIDNmap(context, slave::Cushort, Osize, Isize)
    ccall((:ecx_readIDNmap, libsoem), Cint, (Ptr{ecx_contextt}, Cushort, Ptr{Cint}, Ptr{Cint}), context, slave, Osize, Isize)
end

function ec_config_init(usetable::Cuchar)
    ccall((:ec_config_init, libsoem), Cint, (Cuchar,), usetable)
end

function ec_config_map(pIOmap)
    ccall((:ec_config_map, libsoem), Cint, (Ptr{Void},), pIOmap)
end

function ec_config_overlap_map(pIOmap)
    ccall((:ec_config_overlap_map, libsoem), Cint, (Ptr{Void},), pIOmap)
end

function ec_config_map_group(pIOmap, group::Cuchar)
    ccall((:ec_config_map_group, libsoem), Cint, (Ptr{Void}, Cuchar), pIOmap, group)
end

function ec_config_overlap_map_group(pIOmap, group::Cuchar)
    ccall((:ec_config_overlap_map_group, libsoem), Cint, (Ptr{Void}, Cuchar), pIOmap, group)
end

function ec_config(usetable::Cuchar, pIOmap)
    ccall((:ec_config, libsoem), Cint, (Cuchar, Ptr{Cuchar}), usetable, pIOmap)
end

function ec_config_overlap(usetable::Cuchar, pIOmap)
    ccall((:ec_config_overlap, libsoem), Cint, (Cuchar, Ptr{Void}), usetable, pIOmap)
end

function ec_recover_slave(slave::Cushort, timeout::Cint)
    ccall((:ec_recover_slave, libsoem), Cint, (Cushort, Cint), slave, timeout)
end

function ec_reconfig_slave(slave::Cushort, timeout::Cint)
    ccall((:ec_reconfig_slave, libsoem), Cint, (Cushort, Cint), slave, timeout)
end

function ecx_config_init(context, usetable::Cuchar)
    ccall((:ecx_config_init, libsoem), Cint, (Ptr{ecx_contextt}, Cuchar), context, usetable)
end

function ecx_config_map_group(context, pIOmap, group::Cuchar)
    ccall((:ecx_config_map_group, libsoem), Cint, (Ptr{ecx_contextt}, Ptr{Void}, Cuchar), context, pIOmap, group)
end

function ecx_config_overlap_map_group(context, pIOmap, group::Cuchar)
    ccall((:ecx_config_overlap_map_group, libsoem), Cint, (Ptr{ecx_contextt}, Ptr{Void}, Cuchar), context, pIOmap, group)
end

function ecx_recover_slave(context, slave::Cushort, timeout::Cint)
    ccall((:ecx_recover_slave, libsoem), Cint, (Ptr{ecx_contextt}, Cushort, Cint), context, slave, timeout)
end

function ecx_reconfig_slave(context, slave::Cushort, timeout::Cint)
    ccall((:ecx_reconfig_slave, libsoem), Cint, (Ptr{ecx_contextt}, Cushort, Cint), context, slave, timeout)
end

function ec_sdoerror2string(sdoerrorcode::Cuint)
    ccall((:ec_sdoerror2string, libsoem), Cstring, (Cuint,), sdoerrorcode)
end

function ec_ALstatuscode2string(ALstatuscode::Cushort)
    unsafe_string(ccall((:ec_ALstatuscode2string, libsoem), Cstring, (Cushort,), ALstatuscode))
end

function ec_soeerror2string(errorcode::Cushort)
    ccall((:ec_soeerror2string, libsoem), Cstring, (Cushort,), errorcode)
end

function ecx_elist2string(context)
    ccall((:ecx_elist2string, libsoem), Cstring, (Ptr{ecx_contextt},), context)
end

function ec_elist2string()
    ccall((:ec_elist2string, libsoem), Cstring, ())
end
