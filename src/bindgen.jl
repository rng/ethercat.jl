using Clang.wrap_c

context = wrap_c.init(output_file="libsoem.jl",
                      header_library=x->"libsoem",
                      common_file="libsoem_h.jl",
                      clang_includes=["../SOEM/osal/linux", "../SOEM/oshw/linux", "../SOEM/osal", "../SOEM/oshw"],
                      clang_diagnostics=true)
context.options.wrap_structs = true
context.headers = ["../SOEM/soem/ethercat.h"]
run(context)
#wrap_c.wrap_c_headers(context, ["../SOEM/soem/ethercat.h"])
