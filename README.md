# Basic SOEM EtherCAT Julia Bindings

Wrapper around the [Simple Open EtherCAT Master library](https://github.com/OpenEtherCATsociety/SOEM)

## Installation instructions

    Pkg.add("https://bitbucket.org/rng/Ethercat.jl.git")
    Pkg.build("Ethercat")

## Sample usage

    sudo ~/.julia/v0.6/Ethercat/slaveinfo.jl enp1s0f0
    1 slaves found and configured.

    Slave:1
     Name:EasyCAT 32+32 rev 1
     Output size: 256bits
     Input size: 256bits
     State: 4
     Delay: 0[ns]
     Has DC: 1
     DCParentport:0
     Activeports:1.0.0.0
     Configured address: 1001
     Man: 0000079a ID: 00defede Rev: 00005a01
     SM1 A:1000 L:  32 F:00010064 Type:3
     SM2 A:1200 L:  32 F:00010020 Type:4
     FMMUfunc 0:1 1:2 2:0 3:0
