#!/home/rng/julia/bin/julia

using Ethercat


# Realtime priority stuff, maybe?

mutable struct sched_param
    priority::Cint
end

const SCHED_RR = 2

function sched_setscheduler(sched, prio)
    param = sched_param(prio)
     ccall((:sched_setscheduler, "libc"),
           Cint, (Cint, Cint, Ptr{sched_param}),
           0, sched, pointer_from_objref(param))
end

# mainloop

function loop()
     slave = Ethercat.ec_slave(1)
     slave.state = Ethercat.EC_STATE_OPERATIONAL
     Ethercat.ec_slave!(1, slave)
     Ethercat.ec_writestate(Cushort(1));
     Ethercat.ec_send_processdata()
     Ethercat.ec_receive_processdata(Ethercat.EC_TIMEOUTRET)
     Ethercat.ec_readstate()
     chk = 40
     while chk > 0 && (Ethercat.ec_slave(1).state != Ethercat.EC_STATE_OPERATIONAL)
         Ethercat.ec_send_processdata()
         Ethercat.ec_receive_processdata(Ethercat.EC_TIMEOUTRET)
         Ethercat.ec_readstate()
         Ethercat.ec_statecheck(Cushort(1), Ethercat.EC_STATE_OPERATIONAL, Int32(50000))
         chk -= 1
     end
     if chk == 0
         println("failed to set state of slave")
         return
     end
     println("slave state: ", Ethercat.ec_slave(1).state)

     for i=1:10
         unsafe_store!(Ethercat.ec_slave(1).outputs, i, 1)
         res = Ethercat.ec_send_processdata()
         res = Ethercat.ec_receive_processdata(Ethercat.EC_TIMEOUTRET)
         println(i, " -> ", unsafe_load(Ethercat.ec_slave(1).inputs, 1))
         sleep(0.01)
     end
end

IO_Map = Vector{Cuchar}(4096)

function main()
    ifname = length(ARGS) == 1 ? ARGS[1] : "eth0"
    @show sched_setscheduler(SCHED_RR, 40)
    
    for i=1:4096
        IO_Map[i] = 0
    end

    if Ethercat.ec_init(ifname) > 0
        if Ethercat.ec_config(UInt8(0) , IO_Map) > 0
            Ethercat.ec_configdc()
            while (Ethercat.EcatError()) println(Ethercat.ec_elist2string()) end
            println(Ethercat.ec_slavecount(), " slaves found and configured.")
            Ethercat.ec_statecheck(Cushort(0), Ethercat.EC_STATE_SAFE_OP,
                                   Cint(Ethercat.EC_TIMEOUTSTATE * 3))
            loop()
        else
            println("Couldn't find slaves")
        end
    else
        println("Couldn't open ethernet interface ", ifname)
    end
end

main()
