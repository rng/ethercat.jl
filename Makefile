OBJS = SOEM/soem/ethercatbase.o \
	   SOEM/soem/ethercatcoe.o \
	   SOEM/soem/ethercatconfig.o \
	   SOEM/soem/ethercatdc.o \
	   SOEM/soem/ethercatfoe.o \
	   SOEM/soem/ethercatmain.o \
	   SOEM/soem/ethercatprint.o \
	   SOEM/soem/ethercatsoe.o \
	   SOEM/oshw/linux/nicdrv.o \
	   SOEM/oshw/linux/oshw.o \
	   SOEM/osal/linux/osal.o

CFLAGS = -ISOEM/soem -ISOEM/oshw/linux -ISOEM/osal/linux -ISOEM/osal
TARGET = libsoem.so

all: $(TARGET)

%.o: %.c
	gcc -g -fPIC -shared -Wall -Werror -std=gnu99 $(CFLAGS) -o $@ -c $<

$(TARGET): $(OBJS)
	gcc -shared -fPIC -o $@ $^ -lpthread -lrt

cleanobjs:
	rm -rf $(OBJS)

clean:
	rm -rf $(TARGET) $(OBJS)
