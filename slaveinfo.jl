#!/home/rng/julia/bin/julia

using Ethercat


function chartup2str(tuple)
    return join([Char(x) for x in tuple])
end

function tup_r16(tup, ofs)
    return (((Cint)(tup[ofs+2]) << 8) + 
            ((Cint)(tup[ofs+1])))
end

function tup_r32(tup, ofs)
    return (((Cint)(tup[ofs+4]) << 24) + 
            ((Cint)(tup[ofs+3]) << 16) +
            ((Cint)(tup[ofs+2]) << 8) +
            ((Cint)(tup[ofs+1])))
end

function state_enum_str(s)
    r = ""
    if (s & 0x10) > 0
        s &= ~ 0x10
        r = "ERROR "
    end
    r *= Dict(0=>"NONE", 1=>"INIT", 2=>"PRE_OP", 3=>"BOOT", 4=>"SAFE_OP", 8=>"OPERATIONAL", 16=>"ERROR")[s]
    return r
end

IO_Map = Vector{Cuchar}(4096)

function main()
    ifname = length(ARGS) == 1 ? ARGS[1] : "eth0"

    for i=1:4096
        IO_Map[i] = 0
    end

    if Ethercat.ec_init(ifname) > 0
        if Ethercat.ec_config(UInt8(0) , IO_Map) > 0
            Ethercat.ec_configdc()
            while (Ethercat.EcatError()) println(Ethercat.ec_elist2string()) end
            println(Ethercat.ec_slavecount(), " slaves found and configured.")
            Ethercat.ec_statecheck(Cushort(0), Ethercat.EC_STATE_SAFE_OP,
                                   Cint(Ethercat.EC_TIMEOUTSTATE))
         
            if Ethercat.ec_slave(0).state != Ethercat.EC_STATE_SAFE_OP
                println("Not all slaves reached safe operational state.")
                Ethercat.ec_readstate()
                for i=1:Ethercat.ec_slavecount()
                    if Ethercat.ec_slave(i).state != Ethercat.EC_STATE_SAFE_OP
                        @printf("Slave %d State=%2x StatusCode=%04x : %s\n",
                                i,
                                Ethercat.ec_slave(i).state,
                                Ethercat.ec_slave(i).ALstatuscode,
                                Ethercat.ec_ALstatuscode2string(Ethercat.ec_slave(i).ALstatuscode))
                    end
                end
            end

            Ethercat.ec_readstate()

            for cnt=1:Ethercat.ec_slavecount()
                println()
                @printf("Slave:%d\n Name:%s\n Output size: %dbits\n Input size: %dbits\n",
                        cnt,
                        chartup2str(Ethercat.ec_slave(cnt).name),
                        Ethercat.ec_slave(cnt).Obits,
                        Ethercat.ec_slave(cnt).Ibits)
                @printf(" State: %s\n Delay: %d[ns]\n Has DC: %d\n",
                        state_enum_str(Ethercat.ec_slave(cnt).state),
                        Ethercat.ec_slave(cnt).pdelay,
                        Ethercat.ec_slave(cnt).hasdc)
                
                if (Ethercat.ec_slave(cnt).hasdc != 0)
                    @printf(" DCParentport:%d\n", Ethercat.ec_slave(cnt).parentport)
                end
                
                @printf(" Activeports:%d.%d.%d.%d\n",
                        (Ethercat.ec_slave(cnt).activeports & 0x01) > 0 ,
                        (Ethercat.ec_slave(cnt).activeports & 0x02) > 0 ,
                        (Ethercat.ec_slave(cnt).activeports & 0x04) > 0 ,
                        (Ethercat.ec_slave(cnt).activeports & 0x08) > 0 )
                @printf(" Configured address: %4.4x\n", Ethercat.ec_slave(cnt).configadr)
                @printf(" Man: %8.8x ID: %8.8x Rev: %8.8x\n",
                        Ethercat.ec_slave(cnt).eep_man,
                        Ethercat.ec_slave(cnt).eep_id,
                        Ethercat.ec_slave(cnt).eep_rev)

                for nSM=1:Ethercat.EC_MAXSM
                    SM = Ethercat.ec_slave(cnt).SM[nSM]
                    sm_start = tup_r16(SM.packed, 0)
                    sm_len = tup_r16(SM.packed, 2)
                    sm_flags = tup_r32(SM.packed, 4)
                    if(sm_start > 0)
                        @printf(" SM%1d A:%4.4x L:%4d F:%8.8x Type:%d\n",
                                nSM,
                                sm_start,
                                sm_len,
                                sm_flags,
                                Ethercat.ec_slave(cnt).SMtype[nSM])
                    end
                end

                for j=1:Ethercat.ec_slave(cnt).FMMUunused
                    #@printf(" FMMU%1d Ls:%8.8x Ll:%4d Lsb:%d Leb:%d Ps:%4.4x Psb:%d Ty:%2.2x Act:%2.2x\n", j,
                    #(int)Ethercat.ec_slave[cnt].FMMU[j].LogStart, Ethercat.ec_slave[cnt].FMMU[j].LogLength, Ethercat.ec_slave[cnt].FMMU[j].LogStartbit,
                    #Ethercat.ec_slave[cnt].FMMU[j].LogEndbit, Ethercat.ec_slave[cnt].FMMU[j].PhysStart, Ethercat.ec_slave[cnt].FMMU[j].PhysStartBit,
                    #Ethercat.ec_slave[cnt].FMMU[j].FMMUtype, Ethercat.ec_slave[cnt].FMMU[j].FMMUactive);
                end

                @printf(" FMMUfunc 0:%d 1:%d 2:%d 3:%d\n",
                        Ethercat.ec_slave(cnt).FMMU0func,
                        Ethercat.ec_slave(cnt).FMMU1func,
                        Ethercat.ec_slave(cnt).FMMU2func,
                        Ethercat.ec_slave(cnt).FMMU3func)
            end

        else
            println("Couldn't find slaves")
        end
    else
        println("Couldn't open ethernet interface ", ifname)
    end
end

main()
